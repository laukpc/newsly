//
//  SceneDelegate.swift
//  newsly
//
//  Created by Laurynas Kapacinskas on 2021-09-16.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
      
        let homeVC = HomeViewController()
        homeVC.view.backgroundColor = .white
        homeVC.tabBarItem.title = "Home"
        homeVC.tabBarItem.image = UIImage(systemName: "house")
        
        let newsVC = NewsListViewController()
        newsVC.view.backgroundColor = .white
        newsVC.tabBarItem.title = "News"
        newsVC.tabBarItem.image = UIImage(systemName: "newspaper")
        
        let searchVC = SearchViewController()
        searchVC.view.backgroundColor = .white
        searchVC.tabBarItem.title = "Search"
        searchVC.tabBarItem.image = UIImage(systemName: "magnifyingglass")
        
        let profileVC = ProfileViewController()
        profileVC.view.backgroundColor = .white
        profileVC.tabBarItem.title = "Profile"
        profileVC.tabBarItem.image = UIImage(systemName: "person.crop.circle")
        
        let settingsVC = SettingsViewController()
        settingsVC.view.backgroundColor = .white
        settingsVC.tabBarItem.title = "More"
        settingsVC.tabBarItem.image = UIImage(systemName: "ellipsis.circle.fill")
        
        let tabBarController = TabBarViewController()
        tabBarController.tabBar.tintColor = .orange
        tabBarController.viewControllers = [homeVC, newsVC, searchVC, profileVC, settingsVC]
        
        let window = UIWindow(windowScene: windowScene)
        window.rootViewController = tabBarController 
        window.makeKeyAndVisible()
        self.window = window
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


}

