//
//  APICaller.swift
//  newsly
//
//  Created by Laurynas Kapacinskas on 2021-09-16.
//

import Foundation
import Alamofire

class ApiCaller {
    
    struct Constants {
        static let topHeadlinesURL = URL(string: "https://gnews.io/api/v4/top-headlines?token=e6fafcd6dc9dbb91540f342f3b3f2ce8&lang=en")
    }
    
    static let shared = ApiCaller()
    
    private init() {}
    
    public func getTopArticles(completion: @escaping (Result<[ApiArticle], AFError>) -> Void) {
        guard let url = Constants.topHeadlinesURL else { return }
        AF.request(url, method: .get).responseData(completionHandler: { (response) in
            switch response.result {
            case .success(let res):
                if response.value != nil {
                    let jsonString = String(data: res, encoding: .utf8)!
                    let responseData = Response(JSONString: jsonString)
                    completion(.success(responseData?.articles ?? []))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
}

