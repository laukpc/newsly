//
//  RepositoryManager.swift
//  newsly
//
//  Created by Laurynas Kapacinskas on 2021-09-16.
//

import Foundation
import RealmSwift 

class RepositoryManager {
    var articles = [Article]()
    
    static let shared = RepositoryManager()
    
    private init() {}
    
    func fetchDataFromNetwork(){
        ApiCaller.shared.getTopArticles { (result) in
            switch result {
            case .success(let response):
                self.articles = response.map { article in
                    let articleRealm = Article()
                    articleRealm.title = article.title
                    articleRealm.articleDescription = article.description
                    articleRealm.content = article.content
                    articleRealm.image = article.image
                    articleRealm.url = article.url
                    return articleRealm
                }
                self.updateRealm()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func updateRealm(){
        guard let realm = try? Realm() else { return }
        try? realm.write({
            realm.add(articles, update: .all)
        })
    }
    
    func fetchFromRealm() {
        guard let realm = try? Realm() else { return }
        self.articles = realm.objects(Article.self).map({ article in
            let articleRealm = Article()
            articleRealm.title = article.title
            articleRealm.articleDescription = article.articleDescription
            articleRealm.content = article.content
            articleRealm.image = article.image
            articleRealm.url = article.url
            return articleRealm
        })
    }
    
}
