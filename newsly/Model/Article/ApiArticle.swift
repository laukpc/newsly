//
//  ArticleDB.swift
//  newsly
//
//  Created by Laurynas Kapacinskas on 2021-09-16.
//

import Foundation
import ObjectMapper

// MARK: - Response
struct Response: Codable, Mappable {
    
    var totalArticles: Int?
    var articles = [ApiArticle]()
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        totalArticles <- map["totalArticles"]
        articles <- map["articles"]
    }
}

// MARK: - Article
struct ApiArticle: Codable, Mappable {
    
    var title: String?
    var description: String?
    var content: String?
    var url: String?
    var image: String?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        title <- map["title"]
        description <- map["description"]
        content <- map["content"]
        url <- map["url"]
        image <- map["image"]
    }
}

