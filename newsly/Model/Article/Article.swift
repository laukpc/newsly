//
//  Article.swift
//  newsly
//
//  Created by Laurynas Kapacinskas on 2021-09-16.
//

import Foundation
import RealmSwift

class Article: Object {
    
    @Persisted var title: String?
    @Persisted var articleDescription: String?
    @Persisted var content: String?
    @Persisted var url: String?
    @Persisted var image: String?

    override static func primaryKey() -> String? {
        "url"
    }
}
