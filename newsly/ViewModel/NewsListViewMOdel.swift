//
//  NewsListViewModel.swift
//  newsly
//
//  Created by Laurynas Kapacinskas on 2021-09-22.
//

import Foundation

class NewsListViewModel{
    var articles = [Article]()
    
    init() {
        load()
    }
    
    func load(){
        RepositoryManager.shared.fetchFromRealm()
        articles = RepositoryManager.shared.articles
    }
}
