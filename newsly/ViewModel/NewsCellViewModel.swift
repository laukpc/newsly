//
//  NewsCellViewModel.swift
//  newsly
//
//  Created by Laurynas Kapacinskas on 2021-09-17.
//

import UIKit

class NewsCellViewModel {
    let title: String
    let subtitle: String
    let imageURL: URL?
    let content: String?
    var imageData: Data? = nil
    
    init(
        title: String,
        subtitle: String,
        imageURL: URL?,
        content: String?
        ) {
        self.title = title
        self.subtitle = subtitle
        self.imageURL = imageURL
        self.content = content
    }
}
