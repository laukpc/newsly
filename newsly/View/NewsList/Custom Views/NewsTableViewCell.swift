//
//  NewsTableViewCell.swift
//  NewsTest


import UIKit

class NewsTableViewCell: UITableViewCell {
    static let identifier = "NewsTableViewCell"

    private let placeholderView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()

    private let newsTitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        //label.adjustsFontSizeToFitWidth = true
        label.font = .systemFont(ofSize: 21, weight: .semibold)
        return label
    }()
    
    private let subtitleTitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        //label.adjustsFontSizeToFitWidth = true
        label.font = .systemFont(ofSize: 15, weight: .light)
        return label
    }()
    
    private let newsImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 7
        imageView.layer.masksToBounds = true
        imageView.clipsToBounds = true
        imageView.backgroundColor = .lightGray
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(newsTitleLabel)
        contentView.addSubview(subtitleTitleLabel)
        contentView.addSubview(newsImageView)
        contentView.addSubview(placeholderView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        newsTitleLabel.frame = CGRect(x: contentView.frame.size.width - 260,
                                      y: 25,
                                      width: contentView.frame.size.width - 140,
                                      height: 60
        )
        subtitleTitleLabel.frame = CGRect(x: contentView.frame.size.width - 260,
                                          y: 85,
                                      width: contentView.frame.size.width - 140,
                                      height: contentView.frame.size.height / 3
        )
        newsImageView.frame = CGRect(x: 15,
                                     y: 30,
                                     width: 100,
                                     height: 100
        )
        placeholderView.frame = CGRect(x: 0,
                                       y: 0,
                                       width: contentView.frame.size.width,
                                       height: 10
        )
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        newsTitleLabel.text = nil
        subtitleTitleLabel.text = nil
        newsImageView.image = nil
    }
    
    func configure(with viewModel: NewsCellViewModel){
        newsTitleLabel.text = viewModel.title
        subtitleTitleLabel.text = viewModel.subtitle
        
        if let data = viewModel.imageData {
            newsImageView.image = UIImage(data: data)
        }
        else if let url = viewModel.imageURL {
            //fetch
            URLSession.shared.dataTask(with: url) { [weak self] data, _, error in
                guard let data = data, error == nil else {
                    print("failed to fetch image")
                    return
                }
                viewModel.imageData = data
                DispatchQueue.main.async {
                    self?.newsImageView.image = UIImage(data: data)
                }
            }.resume()
        }
    }
}
