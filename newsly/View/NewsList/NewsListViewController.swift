//
//  NewsListViewController.swift
//  newsly
//
//  Created by Laurynas Kapacinskas on 2021-09-17.
//

import UIKit
import SafariServices

class NewsListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    private var viewModel = NewsListViewModel()
    private var cellViewModels = [NewsCellViewModel]()
    
    private let tableView: UITableView = {
        let table = UITableView()
        table.register(NewsTableViewCell.self, forCellReuseIdentifier: NewsTableViewCell.identifier)
        return table
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorInset = .zero
        tableView.backgroundColor = .white
        prepareViewModels()
    }
    
    private func prepareViewModels(){
        self.cellViewModels = (viewModel.articles.map({
            NewsCellViewModel(
                title: $0.title ?? "",
                subtitle: $0.articleDescription ?? "",
                imageURL: URL(string: $0.image ?? ""),
                content: $0.content ?? ""
                )
            })
        )
        tableView.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
        tableView.separatorStyle = .none
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NewsTableViewCell.identifier, for: indexPath) as? NewsTableViewCell else {
            fatalError("error")
        }
        cell.configure(with: cellViewModels[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let article = viewModel.articles[indexPath.row]
        guard let url = URL(string: article.url ?? "") else { return }
        let vc = SFSafariViewController(url: url)
        present(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        150
    }
}
